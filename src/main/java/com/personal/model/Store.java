package com.personal.model;

import com.personal.repo.ProductRepo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
//@EqualsAndHashCode
public class Store {

    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH})
    private Set<Product> products;

    public Set<Product> getProducts() {
        products = new HashSet<>();
        return products;
    }

    //Many to many bidirectional util class
    public void addProduct(Product product) {
        if (products == null) {
            this.products = new HashSet<>();
        }
        this.products.add(product);
        product.getStores().add(this);
    }

    public void removeProduct(Product product, ProductRepo productRepo) {
        isEqualTo(this.products, product);
        if (this.products.contains(product)) {
            this.products.remove(product);
            product.getStores().remove(this);
        }
    }

    private void isEqualTo(Set<Product> products, Product product) {
        Iterator productsIterator = products.iterator();
        while (productsIterator.hasNext()) {
            Product product1 = (Product) productsIterator.next();
            boolean isEqual = product.equals(product1);
        }

    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Store that = (Store) obj;
        boolean equalStoreId = false;

        if (this.id != null && that.id != null) {
            equalStoreId = this.id == that.id;
        }

        return equalStoreId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
