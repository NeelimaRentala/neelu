package com.personal.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
//@EqualsAndHashCode
public class Product {

    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    //Many to many bidirectional
    @ManyToMany(mappedBy = "products")
    private Set<Store> stores;

    public Set<Store> getStores() {
        if( stores == null){
            stores = new HashSet<Store>();
        }
        return stores;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Product that = (Product) obj;
        boolean equalProductId = false;

        if (this.id != null && that.id != null) {
            equalProductId = this.id == that.id;
        }

        return equalProductId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
