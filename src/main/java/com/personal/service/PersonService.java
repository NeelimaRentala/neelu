package com.personal.service;

import com.personal.domain.ParentPass;
import com.personal.domain.Person;
import org.springframework.stereotype.Service;

@Service
public interface PersonService {
    public Person createPerson();

    public Person getPerson();

    public Person updatePerson();

    public void deletePerson();

    public ParentPass getPass(ParentPass pass);
}
