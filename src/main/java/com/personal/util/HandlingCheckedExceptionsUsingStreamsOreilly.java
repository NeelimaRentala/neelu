package com.personal.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.stream.Collectors;

public class HandlingCheckedExceptionsUsingStreamsOreilly {

    private String encodeString(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return s;
    }

    public String encodedAddressUsingExtractedMethod(String... address) {
        return Arrays.stream(address)
                .map(this::encodeString)
                .collect(Collectors.joining(","));
    }
}
