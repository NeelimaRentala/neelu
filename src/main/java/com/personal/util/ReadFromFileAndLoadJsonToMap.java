package com.personal.util;

import java.io.IOException;
import java.net.URL;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ReadFromFileAndLoadJsonToMap {
    private final ObjectMapper mapper;
    public ReadFromFileAndLoadJsonToMap() {
        mapper = new ObjectMapper();
    }

    private Map load(String source) throws IOException {
        URL url = new URL(source);
        Map<String, Object> map = mapper.readValue(url, Map.class);
        return (Map<String, Map>) map.get("AirportGuidePages");
    }

    public static void main(String[] args) throws IOException {
        String urlMilo = "https://prioritypass.com/en/api/contentapi/airportguides";
        String url = "https://prioritypass.com/en/api/contentapi/airportguides";

        Map airports = new ReadFromFileAndLoadJsonToMap().load(url);
        System.out.println("{");
        for (Object e : airports.entrySet()) {
            Map.Entry<String, Map<String, String>> airport = (Map.Entry<String, Map<String, String>>) e;
            System.out.println("{");
            //System.out.println(airport.getKey());
            System.out.println("\""+airport.getValue().get("AirportCode")+"\",");
            System.out.println("\""+airport.getValue().get("HeroImage")+"\"");
            System.out.println("},");
        }
        System.out.println("}");
    }
}
