package com.personal.util;

//import org.apache.commons.codec.binary.Base64;
import org.apache.tomcat.util.codec.binary.Base64;

import java.nio.ByteBuffer;
import java.util.UUID;

public class Main {

    public static void main(String[] args) {
        long clientId;
        UUID referenceId;

        /*clientId = 1L;
        referenceId = new UUID(clientId, clientId);
        System.out.println(convertToMongoBytes(referenceId));*/

        clientId = 6131001631L;
        referenceId = new UUID(clientId, clientId);
        System.out.println(convertToMongoBytes(referenceId));
    }

    public static String convertToMongoBytes(UUID uuid) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(new byte[16]);
        byteBuffer.putLong(Long.reverseBytes(uuid.getMostSignificantBits()));
        byteBuffer.putLong(Long.reverseBytes(uuid.getLeastSignificantBits()));
        byte[] binaryData = byteBuffer.array();
        return Base64.encodeBase64String(binaryData);
    }
}
