package com.personal.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CustomAnnotationValidator implements
        ConstraintValidator<CustomAnnotation, String> {

    private String date;

    public DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/uu")
            .withResolverStyle(ResolverStyle.SMART);


    @Override
    public boolean isValid(String dateString, ConstraintValidatorContext constraintValidatorContext) {
        LocalDate inputDate;
        try {
            date = dateString;
            inputDate = LocalDate.parse(date, dateFormatter);
        } catch (DateTimeParseException e) {
            return false;
        }
        if( !inputDate.isBefore(LocalDate.now())){
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext.buildConstraintViolationWithTemplate("may not be zero")
                    .addConstraintViolation();
            return false;
        }

        return true;
    }
}