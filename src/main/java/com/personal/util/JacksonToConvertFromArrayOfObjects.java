package com.personal.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.personal.domain.Student;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Slf4j
public class JacksonToConvertFromArrayOfObjects {
    /**
     * Static Method to convert Locations config from file to LocationForProductInventoryItemsDto type.
     *
     * @return LocationForProductInventoryItemsDto
     */
    public static List<Student> getStudentsConverted() {

        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        String fileName = "data/studentsList.txt";
        File file = new File(Objects.requireNonNull(classLoader.getResource(fileName)).getFile());

        log.debug("Reading file from : " + file.toPath());

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            return Arrays.asList(objectMapper.readValue(file, Student[].class));
        } catch (IOException exception) {
            log.error("Exception thrown while converting Students list");
            return null;
        }

    }
}
