package com.personal.mapper;

import com.personal.domain.Address;
import com.personal.domain.Person;
import com.personal.model.AddressModel;
import com.personal.model.PersonModel;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ModelToDtoConverter {

    ModelToDtoConverter INSTANCE = Mappers.getMapper(ModelToDtoConverter.class);

    Person toPerson(final PersonModel personModel);

    PersonModel toPersonModel(final Person person);

    Address toAddress(final AddressModel addressModel);

    AddressModel toAddressModel(final Address address);
}
