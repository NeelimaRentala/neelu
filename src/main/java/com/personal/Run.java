package com.personal;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.personal.handler.PersonHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;

@SpringBootApplication
public class Run {

    /*@Autowired
    private PersonHandler personHandler;

    public static void main(String args[]) {
        SpringApplication.run(Run.class, args);    }*/

    private final ObjectMapper mapper;

    public Run() {
        mapper = new ObjectMapper();
    }

    private Map load(String source) throws IOException {
        URL url = new URL(source);
        Map<String, Object> map = mapper.readValue(url, Map.class);
        return (Map<String, Map>) map.get("AirportGuidePages");
    }

    public static void main(String[] args) throws IOException {
        String url = "https://prioritypass.com/en/api/contentapi/airportguides";
        Map airports = new Run().load(url);
        for (Object e : airports.entrySet()) {
            Map.Entry<String, Map<String, String>> airport = (Map.Entry<String, Map<String, String>>) e;
            System.out.println(airport.getKey());
            System.out.println(airport.getValue().get("AirportCode"));
            System.out.println(airport.getValue().get("HeroImage"));
            //System.out.println("DONE");
        }
        //System.out.println("DONE");
    }

    /*private JsonNode load(String source) throws IOException {
        URL url = new URL(source);
        JsonNode jsonNode = mapper.readTree(url);
        return jsonNode.get("AirportGuidePages");
    }

    public static void main(String[] args) throws IOException {
        String url = "https://prioritypass.com/en/api/contentapi/airportguides";
        JsonNode airports = new Run().load(url);
        Iterator<Map.Entry<String, JsonNode>> it = airports.fields();
        while (it.hasNext()) {
            Map.Entry<String, JsonNode> airport = it.next();
            System.out.println(airport.getKey());
            System.out.println(airport.getValue().get("AirportCode"));
            System.out.println(airport.getValue().get("HeroImage"));
            System.out.println("DONE");
        }
        System.out.println("DONE");
    }*/

}






