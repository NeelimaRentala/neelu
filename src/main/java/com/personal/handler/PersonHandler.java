package com.personal.handler;

import com.personal.domain.ParentPass;
import com.personal.domain.Person;
import com.personal.model.Product;
import com.personal.model.Store;
import com.personal.repo.PersonRepo;
import com.personal.repo.ProductRepo;
import com.personal.repo.StoreRepo;
import com.personal.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class PersonHandler implements PersonService {

    @Autowired
    private PersonRepo personRepo;

    @Autowired
    private StoreRepo storeRepo;

    @Autowired
    private ProductRepo productRepo;

    private Person[] persons;

    //@Autowired
    //private ModelToDtoConverter mapper;

    @Override
    public Person createPerson() {

        /*PersonModel person = PersonModel.builder()
                .name("Nee1")
                .build();

        AddressModel address1 = AddressModel.builder()
                .city("London")
                .street("princeRegent")
                .houseNumber(545)
                .zipCode(500045)
                .build();
        AddressModel address2 = AddressModel.builder()
                .city("Hyd")
                .street("Chandrapuri colony")
                .houseNumber(1000)
                .zipCode(500045)
                .build();

        person.setAddresses(Arrays.asList(address1, address2));
        //person.addAddress(address1);
        //person.addAddress(address2);




        PersonModel p = personRepo.save(person);
        return null;//mapper.toPerson(model);*/

        createProductsAndSaveInStore();
        return null;

    }

    //Call this method to check about JPA uni/bidirectional mant-to-Many mapping
    private Store createProductsAndSaveInStore() {


        //Adding fields - STORE1
        /*Store store1 = Store.builder().id(1).build();
        Product product1 = Product.builder().id(1).build();
        Product product2 = Product.builder().id(2).build();
        Product product3 = Product.builder().id(3).build();
        Product product4 = Product.builder().id(4).build();

        //Many to many - bidirectional
        store1.addProduct(product1);
        store1.addProduct(product2);
        store1.addProduct(product3);
        store1.addProduct(product4);

        //Many to many - Unidirectional
        //store.getProducts().add(product);

        //Many to many - bidirectional
        storeRepo.save(store1);*/

        //STORE2
        /*Store store2 = Store.builder().id(2).build();
        Product product1 = productRepo.getOne(1);
        Product product3 = productRepo.getOne(3);//Product.builder().id(3).build();

        store2.addProduct(product1);
        store2.addProduct(product3);

        storeRepo.save(store2);*/

        //Removing - to remove product2 from store1
        Store store1 = storeRepo.findById(1).get();
        Product product2 = productRepo.findById(2).get();
        deleteProductFromStoreAndDeleteEntityIfOrphan(store1,product2);


        //Remove entire store - should remove the associated products as well
        /*Store store1 = storeRepo.findById(1).get();
        deleteOrphansAndDeleteStore(store1);*/

        return null;

        //Many to many - Unidirectional
        //return productRepo.save(product);


    }

    private void deleteProductFromStoreAndDeleteEntityIfOrphan(Store store, Product product) {
        store.removeProduct(product,productRepo);
        storeRepo.save(store);
        BigInteger isOrphan = productRepo.checkIfEntityIsOrphan(product.getId());
        if( isOrphan.intValue() == 0){
           productRepo.delete(product);
        }

    }

    private void deleteOrphansAndDeleteStore(Store store1) {

        Set<Integer> productsListOfStore1 = storeRepo.getProductIdsOfThisStoreOnly(store1.getId());
        storeRepo.removeThisStoreAssociations(store1.getId());
        productRepo.deleteProducts(productsListOfStore1);
        storeRepo.removeStore(store1.getId());
    }

    @Override
    public Person getPerson() {
        return null;
    }

    @Override
    public Person updatePerson() {
        return null;
    }

    @Override
    public void deletePerson() {

    }

    @Override
    public ParentPass getPass(ParentPass pass) {
        System.out.println(pass.toString());
        return pass;
    }
}
