package com.personal.domain;

import com.personal.model.PersonModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Address implements Serializable {
    private int id;
    private String street;
    private int houseNumber;
    private String city;
    private int zipCode;
    private PersonModel person;
}
