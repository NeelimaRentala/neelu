package com.personal.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
@Builder
public class Child1Pass implements ParentPass  {

    private String child1;

    @Override
    public void getType() {
        System.out.println("child1");
    }

    private Long specificToChild1;
}
