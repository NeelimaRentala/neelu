package com.personal.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@AllArgsConstructor
@NoArgsConstructor
@Data
@SuperBuilder
public class Child2Pass implements ParentPass {

    private String child2;

    @Override
    public void getType() {
        System.out.println("child2");
    }

    private Integer specificToChild2;
}
