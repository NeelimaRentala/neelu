package com.personal.domain;

import java.io.Serializable;

public interface ParentPass extends Serializable {
    long serialVersionUID = 1L;
    public void getType();
}
