package com.personal.domain;

import com.personal.util.CustomAnnotation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Person implements Serializable {

    //@CustomAnnotation - The custom annotation can be included this way
    private Integer id;

    private String name;
    private List<Address> addresses;
}
