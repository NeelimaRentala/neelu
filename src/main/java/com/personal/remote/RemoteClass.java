package com.personal.remote;

import com.collinson.te.common.service.net.adaptor.service.impl.AbstractRemoteService;
import com.personal.domain.Child1Pass;
import com.personal.domain.ParentPass;
import com.personal.domain.Person;
import com.personal.service.PersonService;
import org.springframework.stereotype.Component;

@Component
public class RemoteClass extends AbstractRemoteService implements PersonService {

    public RemoteClass() {
        super("RemoteClass");
    }

    @Override
    public ParentPass getPass(ParentPass pass) {
        return (ParentPass)invokeMethod("getPass",
                buildClasses(ParentPass.class),
                buildParams(pass));
    }

    @Override
    public Person createPerson() {
        return null;
    }

    @Override
    public Person getPerson() {
        return null;
    }

    @Override
    public Person updatePerson() {
        return null;
    }

    @Override
    public void deletePerson() {

    }


}
