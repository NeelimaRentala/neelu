package com.personal.repo;

import com.personal.model.Product;
import com.personal.model.Store;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Repository
public interface StoreRepo extends JpaRepository<Store, Integer> {



    //get all products only of the store which is given as input
    @Query(value = "SELECT sp.products_id FROM store_products sp JOIN " +
            "product p ON sp.products_id = p.id JOIN " +
            "store_products sp2 ON p.id = sp2.products_id WHERE " +
            " sp2.stores_id = :storeId GROUP BY " +
            " sp.products_id HAVING count(sp.stores_id) = 1;" ,
            nativeQuery = true)
    Set<Integer> getProductIdsOfThisStoreOnly(@Param(value = "storeId")Integer storeId);


    //remove all associations for this store
    @Transactional
    @Modifying //This annotation would trigger the query annotated to the method as updating query
    // instead of a selecting one
    @Query(value = "DELETE FROM store_products WHERE stores_id = :storeId" ,
            nativeQuery = true)
    void removeThisStoreAssociations(@Param(value = "storeId")Integer storeId);

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM store WHERE id = :storeId" ,
            nativeQuery = true)
    void removeStore(@Param(value = "storeId")Integer storeId);
}
