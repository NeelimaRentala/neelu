package com.personal.repo;

import com.personal.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.Set;

public interface ProductRepo extends JpaRepository<Product, Integer> {

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM product WHERE id IN (:productIds)" ,
            nativeQuery = true)
    void deleteProducts(@Param(value = "productIds")
                                Set<Integer> productIds);

    @Query(value = "SELECT EXISTS(select * from store_products where products_id = :productId)" ,
            nativeQuery = true)
    BigInteger checkIfEntityIsOrphan(@Param(value = "productId")
                                             Integer productId);
}
