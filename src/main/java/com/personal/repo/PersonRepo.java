package com.personal.repo;

import com.personal.model.PersonModel;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepo extends CrudRepository<PersonModel, Integer> {
    //PersonModel save(PersonModel personModel);
}
