package it;

import com.personal.Run;
import com.personal.domain.Child1Pass;
import com.personal.domain.Child2Pass;
import com.personal.domain.ParentPass;
import com.personal.domain.Person;
import com.personal.handler.PersonHandler;
import com.personal.remote.RemoteClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Run.class)
@TestPropertySource(properties = {"server.port=12345"})
public class PersonIntegrationTest {

    @Autowired
    private PersonHandler personHandler;

    @Autowired
    private RemoteClass remoteClass;

    @Value("${test.string}")
    private String testString;

    @Test
    public void createperson_successful(){
        Person person = personHandler.createPerson();
    }

    @Test
    public void getPass_successful(){

        System.out.println("test string value now is: " + testString);

        Child1Pass child1Pass = new Child1Pass();
        child1Pass.setChild1("1");
        child1Pass.setSpecificToChild1(1l);
        Child2Pass child2Pass = new Child2Pass();
        ParentPass pass1 = personHandler.getPass(child1Pass);
        ParentPass response = remoteClass.getPass(child1Pass);
        /*pass1.getType();
        //child1Pass.getType();
        System.out.println(pass1);
        ParentPass pass2 = personHandler.getPass(child2Pass);
        pass2.getType();
        System.out.println(pass2);*/
    }
}
