DROP TABLE if EXISTS Address;
DROP TABLE if EXISTS Person;

CREATE TABLE persondetails (
  pid bigint NOT NULL AUTO_INCREMENT ,
  name varchar(20),
PRIMARY KEY (pid)
);

CREATE TABLE address(
  aid BIGINT NOT NULL AUTO_INCREMENT,
  person_pid bigint ,
  street varchar(50),
  house_number BIGINT ,
  city varchar(20),
  zip_code varchar(10),
  PRIMARY KEY (aid),
CONSTRAINT FK_ADD FOREIGN KEY (person_pid) REFERENCES  persondetails(pid)
);

